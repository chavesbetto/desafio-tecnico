#!/bin/bash

# Extrair os arquivos do arquivo "desafio.tar"
tar -xf desafio.tar

# Inicializar a string vazia para armazenar as letras encontradas
letters=""

# Iteragir nos arquivos extraídos
for file in $(ls ./desafio); do
  #Apagar README
  rm -rf ./desafio/README*
  # Pegar a segunda linha do arquivo
  letter=$(sed -n 2p ./desafio/$file)
  # Adicionar a letra encontrada na string de resultado
  letters="$letters$letter"
done

# Imprimir a string de letras encontradas
echo $letters