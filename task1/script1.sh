#!/usr/bin/env bash

#
# Script para realizar download e extrair o arquivo  desafio.tar
#
meu_ip=$(hostname -I | cut -f1 -d' ')
curl http://$meu_ip:8888/desafio.tar -o desafio.tar
tar -xvf desafio.tar

