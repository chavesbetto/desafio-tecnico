# DESAFIO-TECNICO

<img src="https://img.shields.io/static/v1?label=docker&message=v20.10.8&color=green&style=for-the-badge&logo=docker"/>
<img src="https://img.shields.io/static/v1?label=python&message=v3.9.7&color=green&style=for-the-badge&logo=python"/>
<img src="https://img.shields.io/static/v1?label=nginx&message=v1.21.3&color=green&style=for-the-badge&logo=nginx"/>
<img src="https://img.shields.io/static/v1?label=licence&message=MIT&color=green&style=for-the-badge&logo=opensourceinitiative.svg"/>

**Conteúdo:**
• [Descrição do Projeto](#descrição-do-projeto) • [Pré-Requisitos](#pré-requisitos) • [Task1-README1](#task1-readm1) • [Task2-README2](#task2-readme2) • [Task3-README3](#task3-readme3) • [Tecnologias](#tecnologias) • [Autor](#autor)

## Descrição do Projeto
<p align="center">Projeto para resolver as questões referente ao desafio técnico.</p>

## Pré-Requisitos

Antes de começar, você vai precisar ter uma VM com SO Linux, preferencialmente CentOS-7, com as seguintes ferramentas:
[Git](https://git-scm.com), [Docker](https://www.docker.com/).

```bash
# Instalar git
$ yum install git

# Instalar docker e inicializar modo Swarm
$ curl -fsSl https://get.docker.com | sh
$ systemctl enable docker && systemctl start docker
$ docker swarm init

# Clonar este projeto
$ git clone https://gitlab.com/chavesbetto/desafio-tecnico.git
$ cd desafio-tecnico
```

### Task1-README1
1- Crie uma estrutura que rode um processo nginx, servindo o arquivo desafio.tar.
Desejável que o processo esteja rodando em um container docker.
```bash
# Build imagem da app
$ docker build -t app-task1 task1/app/

# Deploy da app no swarm
$ docker stack deploy -c task1/docker-compose.yaml task1

# Check containers rodando
$ docker service ls
ID             NAME           MODE         REPLICAS   IMAGE                 PORTS
xjom1ym01kj4   task1_app      replicated   1/1        app-task1:latest      *:8888->80/tcp
```

2- Crie um script que consuma o arquivo desafio.tar da estrutura criada no item 1.
```bash
# Executar script para realizar download do desafio.tar
$ sh task1/script1.sh
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 70656  100 70656    0     0  9014k      0 --:--:-- --:--:-- --:--:-- 11.2M
./._desafio
./desafio/
./desafio/._8500204923122717190914121796.txt
./desafio/8500204923122717190914121796.txt
./desafio/._8024267892099995432956620017.txt
./desafio/8024267892099995432956620017.txt
./desafio/._689214359126731753061165606.txt
./desafio/689214359126731753061165606.txt
./desafio/._20559225942193690552238425855.txt
./desafio/20559225942193690552238425855.txt
./desafio/._3453139232042915125933220726.txt
./desafio/3453139232042915125933220726.txt
./desafio/._196912538211143223661688615688.txt
./desafio/196912538211143223661688615688.txt
./desafio/._29006506911682146542849623810.txt
./desafio/29006506911682146542849623810.txt
./desafio/._909416752993036623180417421.txt
./desafio/909416752993036623180417421.txt
./desafio/._10270201672660726556161878347.txt
./desafio/10270201672660726556161878347.txt
./desafio/._1923135642696929863245129118.txt
./desafio/1923135642696929863245129118.txt
./desafio/._300271399279824984846327498.txt
./desafio/300271399279824984846327498.txt
./desafio/._236583291177905687492928706.txt
./desafio/236583291177905687492928706.txt
./desafio/._66032737823279224251915027009.txt
./desafio/66032737823279224251915027009.txt
./desafio/._68183244615023246462401828469.txt
./desafio/68183244615023246462401828469.txt
./desafio/._7438277071978211406137218972.txt
./desafio/7438277071978211406137218972.txt
./desafio/._5802029222314260031149815594.txt
./desafio/5802029222314260031149815594.txt
./desafio/._297991907780531016776295530.txt
./desafio/297991907780531016776295530.txt
./desafio/._2944245436777159412921728765.txt
./desafio/2944245436777159412921728765.txt
./desafio/._29722177581481223267475811981.txt
./desafio/29722177581481223267475811981.txt
./desafio/._19268631302819762187708004.txt
./desafio/19268631302819762187708004.txt
./desafio/._6563539480493762256482481.txt
./desafio/6563539480493762256482481.txt
./desafio/._1695112728795986351400126650.txt
./desafio/1695112728795986351400126650.txt
./desafio/._720434521829296432802810814.txt
./desafio/720434521829296432802810814.txt
./desafio/._122093264148432177338816393.txt
./desafio/122093264148432177338816393.txt
./desafio/._README1.txt
./desafio/README1.txt
./desafio/._README2.txt
./desafio/README2.txt
./desafio/README3.txt
./desafio/._60553125624357142082950420284.txt
./desafio/60553125624357142082950420284.txt
./desafio/._67572778719543247932887623786.txt
./desafio/67572778719543247932887623786.txt
./desafio/._31595138141032462821017932093.txt
./desafio/31595138141032462821017932093.txt
./desafio/._19521960816351107731949322911.txt
./desafio/19521960816351107731949322911.txt
./desafio/._219968621101762233288637391.txt
./desafio/219968621101762233288637391.txt
```
3- Com o "desafio.tar" já baixado, crie um script que imprima a string "ABC".
Como descrito abaixo, cada arquivo possui duas linhas, sendo a segunda uma das letras [A,B,C].
Não é preciso estar ordenado.
OBS: Os scripts não precisam estar rodando no mesmo container.
```bash
#Executar script para imprimir a string
$ sh task1/script2.sh
tgon hl corbuaa!beaoQaoml r.rn
```

### Task2-README2
1-Configure um servidor nginx com dois server names servindo uma rota chamada "desafio".
Em um dos server names, o retorno para um GET deve ser "desafio1".
Para o outro server name, o retorno para um GET deve ser "desafio2".

```bash
# Build da imagem da app1
$ docker build -t app1-task2 task2/app1/

# Build da imagem da app2
$ docker build -t app2-task2 task2/app2/

# Build da imagem do proxy
$ docker build -t proxy-task2 task2/proxy/

# Deploy do stack com app1, app2 e proxy
$ docker stack deploy -c task2/docker-compose.yaml task2

# Check containers rodando
$ docker service ls
ID             NAME           MODE         REPLICAS   IMAGE                 PORTS
xjom1ym01kj4   task1_app      replicated   1/1        app-task1:latest      *:8888->80/tcp
lswfzf2r4hfm   task2_app1     replicated   1/1        app1-task2:latest     *:30000->80/tcp
r25rtoydpf6t   task2_app2     replicated   1/1        app2-task2:latest     *:30001->80/tcp
xkyx81i06g13   task2_proxy    replicated   1/1        proxy-task2:latest    *:80->80/tcp

#Alterando arquivo /etc/hosts
$ cat <<EOF >>/etc/hosts
$(hostname -I | cut -f1 -d' ') desafio1.desafio
$(hostname -I | cut -f1 -d' ') desafio2.desafio
EOF

# Retorno GET da app1 via proxy
$ curl desafio1.desafio
desafio1

# Retorno GET da app2 via proxy
$ curl desafio2.desafio
desafio2
```

### Task3-README3
1-Crie uma aplicação simples em Python, que retorne “Hello World” ao receber um GET.
2-Configure um nginx que sirva o retorno da aplicação ao receber um GET.
```bash
# Build da imagem da app-python
$ docker build -t python-task3 task3/app-python/

# Build da imagem do proxy
$ docker build -t proxy-task3 task3/nginx/

# Deploy do stack com app-python e proxy
$ docker stack deploy -c task3/docker-compose.yaml task3

# Check containers rodando
$ docker service ls
ID             NAME           MODE         REPLICAS   IMAGE                 PORTS
xjom1ym01kj4   task1_app      replicated   1/1        app-task1:latest      *:8888->80/tcp
lswfzf2r4hfm   task2_app1     replicated   1/1        app1-task2:latest     *:30000->80/tcp
r25rtoydpf6t   task2_app2     replicated   1/1        app2-task2:latest     *:30001->80/tcp
xkyx81i06g13   task2_proxy    replicated   1/1        proxy-task2:latest    *:80->80/tcp
ub70fmrczec9   task3_proxy    replicated   1/1        proxy-task3:latest    *:8889->80/tcp
bdc7e0v93vah   task3_python   replicated   1/1        python-task3:latest   *:30004->8080/tcp

# Retorno GET da app-python via proxy
$ curl $(hostname -I | cut -f1 -d' '):8889
Hello, world!

# Listando os 3 stacks no swarm rodando simultaneamente
$ docker stack ls
NAME      SERVICES   ORCHESTRATOR
task1     1          Swarm
task2     3          Swarm
task3     2          Swarm
```

### Tecnologias

As seguintes ferramentas foram utilizadas para esse projeto:

- [Docker](https://www.docker.com/)
- [Python](https://www.python.org/)
- [Nginx](https://www.nginx.com/)

### Autor
---

 <img styl543109e="border-radius: 50%;" src="https://avatars.githubusercontent.com/u/42034596?s=400&u=daa6bb8af8dfe08f055f1bea7caf4baae658e1e2&v=4" width="100px;" alt=""/>
 <br />
 <b>Beto Chaves</b>


Entre em contato!!

[![Linkedin Badge](https://img.shields.io/badge/-Beto-blue?style=flat-square&logo=Linkedin&logoColor=white&link=https://www.linkedin.com/in/chavesbetto/)](https://www.linkedin.com/in/chavesbetto/) 
[![Gmail Badge](https://img.shields.io/badge/-chavesbetto@gmail.com-c14438?style=flat-square&logo=Gmail&logoColor=white&link=mailto:chavesbetto@gmail.com)](mailto:chavesbetto@gmail.com)
